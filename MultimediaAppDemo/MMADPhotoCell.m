//
//  MMADPhotoCell.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import "MMADPhotoCell.h"

@interface MMADPhotoCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImage;

@end


#import <AssetsLibrary/AssetsLibrary.h>
#import "MMADPhotoCell.h"

@implementation MMADPhotoCell

- (void)setAsset:(ALAsset *)asset
{
    _asset = asset;
    self.photoImage.image = [UIImage imageWithCGImage:asset.thumbnail];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // change to our custom selected background view
    }
    return self;
}

@end
