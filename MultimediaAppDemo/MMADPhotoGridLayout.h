//
//  MMADPhotoGridLayout.h
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 12/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMADPhotoGridLayout : UICollectionViewFlowLayout

@end
