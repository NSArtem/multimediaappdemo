//
//  MMADImageLibrary.h
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMADImageLibrary;

@protocol MMADImageLibraryInformer <NSObject>

@required
- (void)libraryWasUpdatedWithImages:(NSMutableArray*)images;

@end

@interface MMADImageLibrary : NSObject

+ (MMADImageLibrary*)library;

@property(nonatomic, strong, readonly) NSMutableDictionary *images;
@property(nonatomic, strong, readonly) NSMutableDictionary *loadedImages;

//delegate
@property(nonatomic, weak) id<MMADImageLibraryInformer> delegate;

- (void)cacheImageForKey:(NSString*)key;

@end
