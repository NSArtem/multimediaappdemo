//
//  MMADPhotoCell.h
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CELL_NAME @"MMADPhotoCell"

@class ALAsset;

@interface MMADPhotoCell : UICollectionViewCell

@property(nonatomic,weak) ALAsset *asset;

@end
