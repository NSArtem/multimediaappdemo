//
//  MMADPhotoGrid.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 10/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//


#import "MMADPhotoGrid.h"

#import "MMADPhotoGridLayout.h"
#import "MMADPhotoCell.h"
#import "MMADImageLibrary.h"

@interface MMADPhotoGrid ()<MMADImageLibraryInformer>

@property(nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@end

@implementation MMADPhotoGrid   

- (void)viewDidLoad
{
    //delegates
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [MMADImageLibrary library].delegate = self;

    [self.collectionView registerNib:[UINib nibWithNibName:@"MMADPhotoCell" bundle:nil] forCellWithReuseIdentifier:CELL_NAME];
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.5f alpha:1.0f];
    [super viewDidLoad];
}

- (id)init
{
    self.flowLayout = [[MMADPhotoGridLayout alloc] init];
    self = [self initWithCollectionViewLayout:self.flowLayout];
    return self;
}

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithCollectionViewLayout:layout];
    if (self)
    {

    }
    return self;
}

#pragma mark - collection view data source

- (NSInteger) collectionView:(UICollectionView *)collectionView
      numberOfItemsInSection:(NSInteger)section
{
    return [MMADImageLibrary library].images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{    
    MMADPhotoCell *cell =
        [self.collectionView dequeueReusableCellWithReuseIdentifier:CELL_NAME
                                                       forIndexPath:indexPath];
    NSArray *allKeys = [MMADImageLibrary library].images.allKeys;
    NSString *currentKey = allKeys[indexPath.row];
    cell.asset = [MMADImageLibrary library].images[currentKey];
    
    return cell;
}

#pragma mark UICollectionViewDelegate Protocol 

- (void) collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [MMADImageLibrary library].images.allKeys[indexPath.row][0];
    [[MMADImageLibrary library] cacheImageForKey:key];
    
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark MMADImageLibraryInformer
- (void)libraryWasUpdatedWithImages:(NSMutableArray *)images
{
    [self.collectionView reloadData];
}

@end
