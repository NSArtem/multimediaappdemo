//
//  MMADMainViewController.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 9/17/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import "MMADMainViewController.h"
#import "MMADUPnPServer.h"
#import "MMADPhotoGrid.h"

@interface MMADMainViewController ()
- (IBAction)starServerPressed:(id)sender;
- (IBAction)stopServerPressed:(id)sender;
- (IBAction)chooseImagesPressed:(id)sender;

@end

@implementation MMADMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)starServerPressed:(id)sender {
    [[MMADUPnPServer shared] startServer];
}

- (IBAction)stopServerPressed:(id)sender {
}

- (IBAction)chooseImagesPressed:(id)sender {
    MMADPhotoGrid *gridvc = [[MMADPhotoGrid alloc] init];
    [self presentViewController:gridvc animated:YES completion:nil];
}
@end
