//
//  MMADImageLibrary.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreGraphics/CoreGraphics.h>
#import "MMADImageLibrary.h"
#import "MMADUtility.h"

@interface MMADImageLibrary()

@property(nonatomic, strong, readwrite) NSMutableDictionary *images;

@end

@implementation MMADImageLibrary


//TODO: This singleton is redundant
+ (ALAssetsLibrary*)defaultLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

- (id)init
{
    if (self = [super init])
    {
        [self loadAssetsLibrary];
        self.images = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)loadAssetsLibrary
{
    ALAssetsLibrary *library = [[self class] defaultLibrary];
    
    void (^enumerateThroghLibrary)(ALAssetsGroup*, BOOL*) = ^(ALAssetsGroup *group, BOOL *stop)
    {
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if (result)
            {
                NSString *query = [result defaultRepresentation].url.query;
                NSDictionary *components = [query dictionaryFromQueryComponents];
                NSString *key = [components valueForKey:@"id"];
                if (key)
                {
                    [self.images setValue:result forKey:key];
                    NSLog(@"Added image: %@", result);
                }
            }
        }];
        
        //Now saving all photos to directory
        
        [self.delegate performSelector:@selector(libraryWasUpdatedWithImages:)
                            withObject:self.images];
    };
    
    void (^errorEnumerateThroughLibrary)(NSError*) =  ^(NSError* error)
    {
        NSLog(@"Error occured: %@", error);
    };
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:enumerateThroghLibrary
                         failureBlock:errorEnumerateThroughLibrary];

}

+ (MMADImageLibrary*)library
{
    static dispatch_once_t pred = 0;
    static MMADImageLibrary *library;
    dispatch_once(&pred, ^{
        library = [[MMADImageLibrary alloc] init];
    });
    return library;
}

- (void)saveImageToFile:(CGImageRef)image withName:(NSString*)name
{
    if (!name) {
        return;
    }
    NSString *documents = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask,
                                                              YES)[0];
    NSString *filePath = [documents stringByAppendingString:name];

    UIImage *image2 = [UIImage imageWithCGImage:image];
    NSData *data = UIImageJPEGRepresentation(image2, 0.9f);
    [data writeToFile:filePath atomically:YES];
    

}

- (void)cacheImageForKey:(NSString*)key
{
    if (!key) {
        return;
    }
    
    ALAsset *asset = [self.images objectForKey:key];
    
    if (!asset) {
        return;
    }
    
    [self saveImageToFile:[asset.defaultRepresentation fullResolutionImage] withName:key];
}


@end
