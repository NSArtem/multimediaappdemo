//
//  MMADPhotoGrid.h
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 10/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMADPhotoGrid : UICollectionViewController<UICollectionViewDataSource,
                                                      UICollectionViewDelegate,
                                                      UICollectionViewDelegateFlowLayout>

@end
