//
//  MMADUPnPServer.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <Platinum/Platinum.h>
#import "MMADUPnPServer.h"
#import "PltMediaServer.h"

@implementation MMADUPnPServer

+ (MMADUPnPServer *)shared
{
    static dispatch_once_t pred = 0;
    static MMADUPnPServer *server = nil;
    dispatch_once(&pred, ^{
        server = [[MMADUPnPServer alloc] init];
    });
    return server;
}

- (void)startServer
{
    //UPnP engine
    PLT_UPnP upnp;
    
    NSString *documents = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask,
                                                              YES)[0];
    
    char filepath[255];
    
    [documents getCString:filepath maxLength:255 encoding:NSASCIIStringEncoding];
    
    PLT_DeviceHostReference server(new PLT_FileMediaServer(filepath,
                                                           "Platinum UPnP Media Server"));
    
    server->m_ModelDescription = "MERA NN UPnP TEST";
    server->m_ModelURL = "http://www.plutinosoft.com/";
    server->m_ModelNumber = "1.0";
    server->m_ModelName = "Platinum File Media Server";
    server->m_Manufacturer = "Plutinosoft";
    server->m_ManufacturerURL = "http://www.plutinosoft.com/";
    
    // add device
    upnp.AddDevice(server);
    
    NSLog(@"Server started");
}

- (void)stopServer
{

}

@end
