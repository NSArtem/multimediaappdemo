//
//  MMADPhotoGridLayout.m
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 12/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import "MMADPhotoGridLayout.h"

@implementation MMADPhotoGridLayout

- (id)init
{
    self = [super init];
    if (self)
    {
        self.itemSize = CGSizeMake(100, 100);
        self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        self.minimumInteritemSpacing = 10.0f;
        self.minimumLineSpacing = 10.0f;
    }
    return self;
}

@end
