//
//  MMADUPnPServer.h
//  MultimediaAppDemo
//
//  Created by Artem Abramov on 11/09/13.
//  Copyright (c) 2013 Mera Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMADUPnPServer : NSObject

+ (MMADUPnPServer*)shared;

- (void)startServer;
- (void)stopServer;

@end
